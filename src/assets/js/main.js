function setSlickSlider (elem, param) {
	let slick = elem.slick({
		dots:            param.dots,
		draggable:       param.draggable,
		arrows:          param.arrows,
		prevArrow:       param.prevArrow,
		nextArrow:       param.nextArrow,
		infinite:        param.infinite,
		slidesToShow:    param.slidesToShow,
		responsive:      param.responsive,
		verticalSwiping: param.verticalSwiping,
		vertical:        param.vertical,
		autoplay:        param.autoplay,
		autoplaySpeed:   param.autoplaySpeed,
	});

	return slick;
}

function setSlick () {
	setSlickSlider($('#main-slider'), {
		dots:      true,
		arrows:    true,
		draggable: false,
		prevArrow: $('.home__top-arrows-prev'),
		nextArrow: $('.home__top-arrows-next'),
	});

	setSlickSlider($('#v-game-list'), {
		vertical:        true,
		slidesToShow:    5,
		slidesToScroll:  5,
		verticalSwiping: true,
		draggable:       true,
		infinite:        true,
		arrows:          false,
		autoplay:        true,
		autoplaySpeed:   5000
	});

	setSlickSlider($('#game-category-slider'), {
		arrows:         true,
		infinite:       false,
		slidesToShow:   7,
		slidesToScroll: 1,
		responsive:     [{
			breakpoint: 1201,
			settings:   {
				slidesToShow:   7,
				slidesToScroll: 1,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow:   6,
				slidesToScroll: 1,
			}
		}, {
			breakpoint: 1099,
			settings:   {
				slidesToShow:   5,
				slidesToScroll: 1,
			}
		}, {
			breakpoint: 1024,
			settings:   {
				slidesToShow:   4,
				slidesToScroll: 1,
			}
		}, {
			breakpoint: 800,
			settings:   {
				slidesToShow:   3,
				slidesToScroll: 2,
				arrows:         true,
				dots:           true,
				infinite:       true
			}
		},
			{
				breakpoint: 600,
				settings:   {
					slidesToShow:   2,
					slidesToScroll: 2,
					arrows:         true,
					dots:           true,
				}
			},
			{
				breakpoint: 320,
				settings:   {
					slidesToShow:   1,
					slidesToScroll: 1,
					arrows:         true,
					dots:           true,
				}
			}
		]
	});

	setSlickSlider($('#winner-slider'), {
		arrows:         false,
		dots:           false,
		infinite:       false,
		autoplay:       true,
		slidesToShow:   6,
		slidesToScroll: 1,
		responsive:     [{
			breakpoint: 1201,
			settings:   {
				slidesToShow: 5,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 1099,
			settings:   {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 1024,
			settings:   {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings:   {
				slidesToShow: 3,
			}
		},
			{
				breakpoint: 600,
				settings:   {
					slidesToShow: 1.5,
				}
			}
		]
	});

	// mouse wheel scroll for winners slider

	let scrollCount = null;
	let scroll = null;
	$('#winner-slider').on('wheel', (function (e) {
		e.preventDefault();

		clearTimeout(scroll);
		scroll = setTimeout(function () {
			scrollCount = 0;
		}, 200);
		if (scrollCount) return 0;
		scrollCount = 1;

		if (e.originalEvent.deltaY < 0) {
			$(this).slick('slickNext');
		} else {
			$(this).slick('slickPrev');
		}
	}))
}


// navbar lang switch dropdown
function langDropdown () {
	$('.header__lang-bar').click(function (e) {
		e.stopPropagation();
		$(this).toggleClass('open');
	})
	$('body', 'html').click(function () {
		$('.header__lang-bar').removeClass('open');
	})
	$('.header__lang-bar-box').click(function (e) {
		e.stopPropagation();
	})
}

// navbar responsive menu
function hamburger () {
	$('.hamburger').click(function () {
		$(this).closest('.header').addClass('open');
	});
	$('.mobile-cls').click(function () {
		$('.header').removeClass('open')
	})
	$('.header').find('.overlay').click(function () {
		$('.header').removeClass('open')
	})
}

// home page providers list
function providersDropdown () {
	$('.providers-sort-btn').click(function () {
		$(this).closest('.game__providers ').toggleClass('open')
	})
}

// home page game loader
function loaderBtn () {
	$('.loader').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('active')
	})
}


function bsModalBodyFix () {  //prevent body scroll on modal show
	$('.modal').on('hidden.bs.modal', function () {
		if ($(".modal:visible").length > 0) {
			setTimeout(function () {
				$('body').addClass('modal-open');
			}, 200)
		}
	});
}

//redirect url to current tab
function tabLocation () {
	let url = location.href.replace(/\/$/, "");

	if (location.hash) {
		const hash = url.split("#");
		$('#user-fields a[href="#' + hash[1] + '"]').tab("show");
		url = location.href.replace(/\/#/, "#");
		history.replaceState(null, null, url);
		setTimeout(() => {
			$(window).scrollTop(0);
		}, 400);
	}

	$('a[data-toggle="tab"]').on("click", function () {
		let newUrl;
		const hash = $(this).attr("href");
		if (hash == "#account") {
			newUrl = url.split("#")[0];
		} else {
			newUrl = url.split("#")[0] + hash;
		}
		newUrl += " ";
		history.replaceState(null, null, newUrl);
	});
};

// navbar dropdown function
function navUserDropdown () {
	$('.header .nav-user-btn').click(function () {
		$(this).closest('.nav-user').toggleClass('open');
	});
	$('.header .nav-user').click(function (e) {
		e.stopPropagation();
	})
	$('body', 'html').click(function () {
		$('.header .nav-user').removeClass('open');
	})
}

function sidebarTabs () {  // 04/03/20 update
	$('.md-account-nav .nav-item a').click(function () {
		$('.header').removeClass('open');
	})
}

function scrollFixCategory () { // 04/03/20 update
	let $obj = $('.game__category');
	let top = $('.home__top').height();

	$(window).resize(function () {
		top = $('.home__top').height();
	})

	$(window).scroll(function (event) {
		let y = $(this).scrollTop();

		if (y >= top) {
			$obj.addClass('fixed');
			$('.game__providers').css('marginTop', "110px")
		} else {
			$obj.removeClass('fixed');
			$('.game__providers').css('marginTop', '0')
		}
	});
}

function showTopWinnersOnScroll(){
	let winners = $('.top-winners');
	let top = $('#games__cont--new').offset().top;
	$(window).scroll(function (event) {
		let y = $(this).scrollTop();
		if (y >= top) {
			winners.addClass('show');
		} else {
			winners.removeClass('show');
		}
	});
}

function hideTopWinners(){
	$('.hide_btn').click(function () {
		$(this).find('span').text(function(i, text){
			return text === "winning now" ? "hide" : "winning now";
		})
		$(this).closest('.top-winners').toggleClass('active');
	})
}

$(function () {
	langDropdown();
	hamburger();
	setSlick();
	providersDropdown();
	bsModalBodyFix();
	tabLocation();
	navUserDropdown(); //just to see how it works
	loaderBtn(); //just to see how it works
	sidebarTabs(); // 04/03/20 update
	scrollFixCategory(); // 04/03/20 update
	showTopWinnersOnScroll(); // 04/14/20 update
	hideTopWinners(); // 04/14/20 update
});
